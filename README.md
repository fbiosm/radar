Radar Parlamentar
==================

[![build status](https://gitlab.com/radar-parlamentar/radar/badges/master/build.svg)](https://gitlab.com/radar-parlamentar/radar/commits/master)

Este projeto utiliza dados abertos para analisar os votos de parlamentares em
diferentes casas legislativas do Brasil.

O Radar Parlamentar determina "semelhanças" entre partidos políticos baseado na
análise matemática dos dados de votações de projetos de lei na casa
legislativa. Essas semelhanças são apresentadas em um gráfico bi-dimensional,
em que círculos representam partidos e a distância entre esses círculos
representa o quão parecido esses partidos votam.

Site em produção:
[Radar Parlamentar](http://radarparlamentar.polignu.org/ "Radar Parlamentar")

Wiki: https://gitlab.com/radar-parlamentar/radar/wikis

Configuração do ambiente:
[Setup](https://gitlab.com/radar-parlamentar/radar/blob/master/doc/SETUP.md)

## Arquitetura Tecnológica

Do ponto de vista de tecnologia o Radar Parlamentar é desenvolvido em Python,
utilizando o framework Django.

Outras ferramentas e tecnologias que utilizamos são:

  - nginx
  - rabbitmq
  - postgres
  - elasticsearch
  - memcache
  - Celery

O projeto também é todo desenvolvido utilizando containers, e sua arquitetura
básica é:

```mermaid
graph LR

DJ(Aplicação Python/Django)
MC(Memcache)
NG(Nginx)
RB(RabbitMQ)
PQ(Postgres)
ES(Elasticsearch)
CE(Python/Celery)

DJ --> MC
DJ --> NG
DJ --> RB
DJ --> PQ
DJ --> ES
RB --> CE
PQ --> CE
ES --> CE
```

## Artigos e Referências

Artigo acadêmico sobre o Radar: https://www.revistas.usp.br/leviathan/article/view/143408

Licença do projeto: *AGPL v3*

Seja bem vindo!
