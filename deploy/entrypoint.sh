#!/usr/bin/env sh
TRY_LOOP="20"

# Não checa nada se estivermos em ambiente de teste, visto que, POR ENQUANTO,
# estamos usando sqlite para testes.
export DB_HOST="postgres"
export DB_PORT="5432"

wait_for_port() {
  local name="$1" host="$2" port="$3"
  local j=0
  while ! nc -z "$host" "$port" >/dev/null 2>&1 < /dev/null; do
    j=$((j+1))
    if [ $j -ge $TRY_LOOP ]; then
      echo >&2 "$(date) - $host:$port still not reachable, giving up"
      exit 1
    fi
    echo "$(date) - waiting for $name... $j/$TRY_LOOP"
    sleep 5
  done
}

cleanup() {
    # Cleanup instance files, such as socket files, test files and so on.
    echo "Doing exit cleanup."
    echo "  - Removing /radar/sockets folder."
    rm -rf /radar/sockets
    rm -rf /radar/radar_parlamentar/static
    rm -rf /radar/radar_parlamentar/htmlcov
}

setup_cron() {
  echo "Setuping cron"
  crontab -l > mycron
  # Executes 'manage.py runcrons' hourly:
  echo "5 * * * * python /radar/radar_parlamentar/manage.py runcrons >> /var/log/radar/cronjob.log 2>&1" >> mycron
  crontab mycron
  rm mycron
  crond
}

setup_pgpass() {
  echo "Setuping pgpass"
  echo "postgres:5432:radar:radar:${RADAR_DB_PASSWORD}" > $PGPASSFILE
  chmod 600 $PGPASSFILE
}

compress_files() {
    # Compress js and css files.
    echo "Compressing js and css files"
    python manage.py compress
}

case "$1" in
  deploy)
    echo "Initializing deploy mode."
    wait_for_port "Postgres" "$DB_HOST" "$DB_PORT"
    python manage.py migrate
    python manage.py collectstatic --noinput
    compress_files
    setup_pgpass
    setup_cron
    ;;
  celery)
    echo "Initializing celery."
    celery -A radar_parlamentar worker -l info --concurrency 1
    ;;
  clean)
    echo "Initializing cleaning mode."
    cleanup
    ;;
  test|test_and_coverage)
    echo "Initializing test and coverage mode."
    # Radar will use SQLite
    export RADAR_TEST='True'
    coverage run --rcfile="../.coveragerc" --source="." manage.py test analises exportadores importadores modelagem plenaria radar_parlamentar
    echo ""
    echo ""
    echo "################################################################"
    echo "Coverage report:"
    coverage report
    coverage html &> /dev/null
    echo "To see the detailed coverage report, open the file radar_parlamentar/htmlcov/index.html on your browser."
    ;;
  *)
    # The command is something like bash, not an airflow subcommand. Just run it in the right environment.
    echo "Default initialization."
    wait_for_port "Postgres" "$DB_HOST" "$DB_PORT"
    echo "Starting uwsgi"
    uwsgi --ini /radar/deploy/radar_uwsgi.ini
    ;;
esac
