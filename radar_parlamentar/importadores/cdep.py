# Copyright (C) 2012, Leonardo Leite, Diego Rabatone, Saulo Trento,
# Carolina Ramalho, Brenddon Gontijo Furtado
#
# This file is part of Radar Parlamentar.
#
# Radar Parlamentar is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Radar Parlamentar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Radar Parlamentar.  If not, see <http://www.gnu.org/licenses/>.

"""módulo que cuida da importação dos dados da Câmara dos Deputados"""

from django.core.exceptions import ObjectDoesNotExist
from .chefes_executivos import ImportadorChefesExecutivos
from modelagem import models
from datetime import datetime
import urllib.request
import urllib.error
import urllib.parse
import logging
import json
import os
import time

##########
# Legado #
##########
import xml.etree.ElementTree as etree
import zeep
from zeep.exceptions import TransportError



MODULE_DIR = os.path.abspath(os.path.dirname(__file__))
RESOURCES_FOLDER = os.path.join(MODULE_DIR, 'dados/cdep/')

ANO_MIN = 1991
# só serão buscadas votações a partir de ANO_MIN

logger = logging.getLogger("radar")
logger.setLevel(logging.INFO)

XML_FILE = 'dados/chefe_executivo/chefe_executivo_congresso.xml.bz2'
NOME_CURTO = 'cdep'

def convert_none_to_string(value):
    return value or ''

class Camaraws:
    """ Client SOAP para acessar a API antiga da Câmara.
        As requisições podem ser feitas no estilo:
        client_proposicao.service.ObterProposicaoPorID("13345")
        - basta trocar pela requisição desejada.
    """
    URL_PROPOSICAO = 'https://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx?wsdl'

    """Acesso aos Web Services da Camara dos Deputados"""
    # API antiga pois a nova nao oferece a funcionalidade
    URL_PLENARIO = 'http://www.camara.gov.br/SitCamaraWS/' + \
                   'Proposicoes.asmx/ListarProposicoesVotadasEmPlenario?'

    def obter_proposicao_por_id(self, id_prop):
        """Obtém detalhes de uma proposição

        Argumentos:
        id_prop

        Retorna:
        Um objeto correspondente ao XML retornado pelo web service

        Exceções:
            zeep.exceptions.TransportError -- quando proposição não existe
        """

        soap_client = zeep.Client(wsdl=self.URL_PROPOSICAO)

        """Acesso aos Web Services da Camara dos Deputados"""
        service_proposicao_por_id = soap_client.service.ObterProposicaoPorID

        try:
            proposicao = service_proposicao_por_id(id_prop)
        except TransportError:
            raise ValueError('Proposicao %s nao encontrada' % id_prop)


        return proposicao

    def obter_votacoes(self, id_prop):
        """
        Obtem votacoes de uma proposicao

        Argumentos:
        id_prop: id unico que caracteriza uma proposicao

        Retorna:
        Uma lista com as votacoes processadas retornadas pelo Web Service

        Excecoes:
        zeep.exceptions.TransportError -- quando proposicao nao existe
        """
        soap_client = zeep.Client(wsdl=self.URL_PROPOSICAO)

        """Acesso aos Web Services da Camara dos Deputados"""
        service_votacoes_por_id = soap_client.service.ObterVotacaoProposicaoPorID
        
        """
        A resposta vem em um formato como:
        <proposicao>
            <votacoes>
                <votacao>
                <votacao>
            </votacoes>
        </proposicao>
        """

        try:
            votacoes_root = service_votacoes_por_id(id_prop) # corresponde ao elemento <proposicao>        
        except TransportError:
            raise ValueError('Votacoes da proposicao %s nao encontrada' % id_prop)

        votacoes_list = votacoes_root.find('Votacoes') # corresponde ao elemento <votacoes>

        return votacoes_list.findall('Votacao') # retorna uma lista com vários <votacao>

    def obter_proposicoes_votadas_plenario(self,ano):
        """
        Obtem as votações votadas em Plenario

        Argumentos:
        obrigatorio : ano

        Retorna:
        Um objeto ElementTree correspondente ao XML retornado pelo web service
        Exemplo:
        http://www.camara.gov.br/sitcamaraws/Proposicoes.asmx/ListarProposicoesVotadasEmPlenario?ano=1991&tipo=
        """

        proposicoes = None
        soap_client = zeep.Client(wsdl=self.URL_PROPOSICAO)
        service_proposicoes_votadas_plenario = soap_client.service.ListarProposicoesVotadasEmPlenario

        try:
            proposicoes = service_proposicoes_votadas_plenario(ano)
        except zeep.exceptions.TransportError as erro:
            raise ValueError('O ano {} nao possui votacoes ainda'.format(ano))

        return proposicoes

    @staticmethod
    def listar_siglas():
        """Listar as siglas de proposições existentes; exemplo: "PL", "PEC" etc.
        O retorno é feito em uma lista de strings.
        """
        # A lista completa se encontra aqui:
        # https://dadosabertos.camara.leg.br/api/v2/referencias/tiposProposicao
        # No entanto, muito dessas siglas correspondem a proposições que
        # não possuem votações
        # Por isso estamos aqui retornando um resultado mais restrito
        return ['PL', 'MPV', 'PDC', 'PEC', 'PLP',
                'PLC', 'PLN', 'PLOA', 'PLS', 'PLV']

    @staticmethod
    def listar_siglas_string():
        """Listar as siglas de proposições existentes; exemplo: "PL", "PEC" etc.
        O retorno é feito em uma string com as siglas separadas por virgulas.
        """
        # A lista completa se encontra aqui:
        # https://dadosabertos.camara.leg.br/api/v2/referencias/tiposProposicao
        # No entanto, muito dessas siglas correspondem a proposições que
        # não possuem votações
        # Por isso estamos aqui retornando um resultado mais restrito
        return 'PL,MPV,PDC,PEC,PLP,PLC,PLN,PLOA,PLS,PLV'

class ProposicoesFinder:
    """
    Classe legada, a nova API nao oferece a mesma funcionalidade.
    """

    def __init__(self, camaraws):
        self.camaraws = camaraws

    def find_props_disponiveis(self, ano_max=None, ano_min=ANO_MIN):
        """Retorna uma lista com proposicoes que tiveram votações
        entre ano_min e ano_max.
        Cada votação é um dicionário com chaves \in {id, sigla, num, ano}.
        As chaves e valores desses dicionários são strings.

        ano_min padrão é 1991
        """

        if ano_max is None:
            ano_max = datetime.today().year

        proposicoes_votadas = []
        for ano in range(ano_min, ano_max + 1):
            logger.info('Procurando em %s' % ano)
            try:
                proposicoes = self.camaraws.obter_proposicoes_votadas_plenario(ano)
                proposicoes = self._parse_xml(proposicoes)
                proposicoes_votadas.extend(proposicoes)
                logger.info('%d proposições encontradas' %
                            len(proposicoes))
            except Exception as e:
                logger.error(e)
        
        return proposicoes_votadas[::-1]


    def _parse_xml(self, xml):
        prop_votadas = []
        for child in xml:
            id_prop = child.find('codProposicao').text.strip()
            nome_prop = child.find('nomeProposicao').text.strip()
            dic_prop = self._build_dic(id_prop, nome_prop)
            prop_votadas.append(dic_prop)
        return prop_votadas

    @staticmethod
    def _build_dic(id_prop, nome_prop):
        sigla = nome_prop[0:nome_prop.index(" ")]
        num = nome_prop[nome_prop.index(" ") + 1: nome_prop.index("/")]
        ano = nome_prop[nome_prop.index("/") + 1: len(nome_prop)]
        return {'id': id_prop, 'sigla': sigla, 'num': num, 'ano': ano}


def _converte_data(data_str):
    """Converte string 'a-m-dTh:min' para objeto datetime.date;
    retona None se data_str é inválido
    """
    try:
        return  datetime.strptime(data_str, '%d/%m/%Y')
    except ValueError:
        return None

class ImportadorCamara:
    """Salva os dados dos web services da
    Câmara dos Deputados no banco de dados"""

    def __init__(self, camaraws):
        self.camaraws = camaraws
        self.camara_dos_deputados = self._gera_casa_legislativa()
        self.parlamentares = self._init_parlamentares()
        self.proposicoes = self._init_proposicoes()
        self.votacoes = self._init_votacoes()
        self.opcoes_voto_desconhecidas = []
        self.partidos_desconhecidos = []

    def _gera_casa_legislativa(self):
        """Gera objeto do tipo CasaLegislativa
        Câmara dos Deputados e o salva no banco de dados.
        Caso cdep já exista no banco de dados, retorna o objeto já existente.
        """
        count_cdep = models.CasaLegislativa.objects.filter(
            nome_curto='cdep').count()
        if not count_cdep:
            camara_dos_deputados = models.CasaLegislativa()
            camara_dos_deputados.nome = 'Câmara dos Deputados'
            camara_dos_deputados.nome_curto = 'cdep'
            camara_dos_deputados.esfera = models.FEDERAL
            camara_dos_deputados.save()
            return camara_dos_deputados
        else:
            return models.CasaLegislativa.objects.get(nome_curto='cdep')

    def _init_parlamentares(self):
        """(nome_parlamentar,nome_partido,localidade) -> Parlamentar"""
        parlamentares = {}
        for p in models.Parlamentar.objects.filter(
                casa_legislativa=self.camara_dos_deputados):
            parlamentares[self._key_parlamentar(p)] = p
        return parlamentares

    def _key_parlamentar(self, parlamentar):
        return (parlamentar.nome,
                parlamentar.partido.nome,
                parlamentar.localidade)

    def _init_proposicoes(self):
        """id_prop -> Proposicao"""
        proposicoes = {}
        for p in models.Proposicao.objects.filter(
                casa_legislativa=self.camara_dos_deputados):
            proposicoes[p.id_prop] = p
        return proposicoes

    def _init_votacoes(self):
        """(id_prop,descricao,data) -> Votacao"""
        votacoes = {}
        for v in models.Votacao.objects.filter(
                proposicao__casa_legislativa=self.camara_dos_deputados):
            votacoes[self._key_votacao(v)] = v
        return votacoes

    def _key_votacao(self, votacao):
        return (votacao.proposicao.id_prop, votacao.descricao, votacao.data)

    def importar(self, votadas):
        """votadas -- lista de dicionários com
            id/sigla/num/ano das proposições que tiveram votações
        """
        
        self.total_proposicoes = len(votadas)
        self.proposicoes_importadas = 0
        self.imprimir_quando_progresso = 5
        for proposicao in votadas:
            logger.info(proposicao)

            #time.sleep(1)
            self._importar(proposicao)
            self._progresso()

    def _progresso(self):
        self.proposicoes_importadas += 1
        fracao = self.proposicoes_importadas / self.total_proposicoes
        porcentagem = 100.0 * fracao
        if porcentagem > self.imprimir_quando_progresso:
            logger.info('Progresso: %.1f%%' % porcentagem)
            self.imprimir_quando_progresso += 5

    def _importar(self, dic_proposicao):
        """dic_proposicao -- dicionário com
            id/sigla/num/ano de uma proposição a ser importada
        """
        id_prop = dic_proposicao['id']
        sigla = dic_proposicao['sigla']
        num = dic_proposicao['num']
        ano = dic_proposicao['ano']

        try:
            if id_prop in self.proposicoes:
                prop = self.proposicoes[id_prop]
            else:
                prop_from_xml = self.camaraws.obter_proposicao_por_id(id_prop)
                prop = self._prop_from_xml(prop_from_xml)

            for votacao in self.camaraws.obter_votacoes(id_prop):
                self._votacao_from_xml(votacao, prop)
        except ValueError as error:
            logger.error("ValueError: %s" % error)

    def _prop_from_xml(self, prop_xml):
        """prop_xml -- tipo etree

        Retorna proposicao
        """
        id_prop = prop_xml.find('idProposicao').text.strip()
        prop = models.Proposicao()
        prop.id_prop = id_prop
        prop.sigla = prop_xml.get('tipo').strip()
        prop.numero = prop_xml.get('numero').strip()
        prop.ano = prop_xml.get('ano').strip()
        logger.info("Importando %s %s/%s" % (
            prop.sigla, prop.numero, prop.ano))
        prop.ementa = prop_xml.find('Ementa').text.strip()
        prop.descricao = convert_none_to_string(prop_xml.find('ExplicacaoEmenta').text).strip()
        prop.indexacao = convert_none_to_string(prop_xml.find('Indexacao').text).strip()
        prop.autor_principal = convert_none_to_string(prop_xml.find('Autor').text).strip()
        date_str = convert_none_to_string(prop_xml.find('DataApresentacao').text).strip()
        prop.data_apresentacao = _converte_data(date_str)
        prop.situacao = convert_none_to_string(prop_xml.find('Situacao').text).strip()
        prop.casa_legislativa = self.camara_dos_deputados
        prop.save()
        self.proposicoes[id_prop] = prop
        return prop

    def _votacao_from_xml(self, votacao_xml, prop):
        """votacao_xml -- XML representando votação (objeto etree)
           prop -- objeto do tipo Proposicao
        """
        descricao = 'Resumo: [%s]. ObjVotacao: [%s]' % (
            votacao_xml.get('Resumo'), votacao_xml.get('ObjVotacao'))
        data_str = convert_none_to_string(votacao_xml.get('Data')).strip()
        data = _converte_data(data_str)

        key = (prop.id_prop, descricao, data)
        if key not in self.votacoes:
            votacao = models.Votacao()
            votacao.proposicao = prop
            votacao.descricao = descricao
            votacao.data = data
            votacao.save()
            self.votacoes[key] = votacao
            if votacao_xml.find('votos') is not None:
                for voto_xml in votacao_xml.find('votos'):
                    self._voto_from_xml(voto_xml, votacao)
            votacao.save()


    def _voto_from_xml(self, voto_xml, votacao):
        """voto_xml -- XML representando voto (objeto etree)
           votacao -- objeto do tipo Votacao
        """
        opcao_str = voto_xml.get('Voto')
        deputado = self._deputado(voto_xml)
        voto = models.Voto()
        voto.opcao = self._opcao_xml_to_model(opcao_str)
        voto.parlamentar = deputado
        voto.votacao = votacao
        voto.save()


    def _opcao_xml_to_model(self, voto):

        voto = voto.strip()

        if voto == 'Não':
            return models.NAO
        elif voto == 'Sim':
            return models.SIM
        elif voto == 'Obstrução':
            return models.OBSTRUCAO
        elif voto == 'Abstenção':
            return models.ABSTENCAO
        elif voto == 'Art. 17':
            return models.ABSTENCAO
        else:
            if not voto in self.opcoes_voto_desconhecidas:
                self.opcoes_voto_desconhecidas.append(voto)
                logger.warning(
                    'opção de voto "%s" desconhecido! Mapeado como ABSTENCAO'
                    % voto)
            return models.ABSTENCAO

    def _deputado(self, voto_xml):
            """Procura primeiro no cache e depois no banco; se não existir,
            cria novo parlamentar"""
            nome = voto_xml.get('Nome')
            nome_partido = voto_xml.get('Partido')
            partido = self._partido(nome_partido)
            localidade = voto_xml.get('UF')
            key = (nome, partido.nome, localidade)
            parlamentar = self.parlamentares.get(key)
            if not parlamentar:
                parlamentar = models.Parlamentar()
                parlamentar.id_parlamentar = voto_xml.get('ideCadastro')
                parlamentar.nome = nome
                parlamentar.partido = partido
                parlamentar.localidade = localidade
                parlamentar.casa_legislativa = self.camara_dos_deputados
                parlamentar.save()
                if partido.numero == 0:
                    if not nome_partido in self.partidos_desconhecidos:
                        self.partidos_desconhecidos.append(nome_partido)
                        logger.warn('Não achou o partido %s. Mapeado como "sem partido"'
                            % nome_partido)
                self.parlamentares[key] = parlamentar
            return parlamentar


    def _partido(self, nome_partido):
        nome_partido = nome_partido
        partido = models.Partido.from_nome(nome_partido)
        if partido is None:
            partido = models.Partido.get_sem_partido()
        return partido


class PosImportacao:

    def processar(self):

        self.remover_votacao_com_deputados_sem_partidos()

    # Issue #256
    def remover_votacao_com_deputados_sem_partidos(self):
        try:
            prop = models.Proposicao.objects.get(sigla='PL',
                                                 numero='821',
                                                 ano='1995')
            obj_votacao = 'SUBEMENDA A EMENDA N. 33'
            votacao = models.Votacao.objects.get(
                proposicao=prop, descricao__contains=obj_votacao)
            votacao.delete()
        except ObjectDoesNotExist:
            logger.warn('Votação esperada (em PL 821/1995)\
                        não foi encontrada na base de dados.')

def main():
    logger.info('IMPORTANDO DADOS DA CAMARA DOS DEPUTADOS')
    camaraws = Camaraws()

    finder = ProposicoesFinder(camaraws)
    dic_votadas = finder.find_props_disponiveis()
    importador = ImportadorCamara(camaraws)
    importador.importar(dic_votadas)
    pos_importacao = PosImportacao()
    pos_importacao.processar()
    logger.info('IMPORTANDO CHEFES EXECUTIVOS DA CAMARA DOS DEPUTADOS')
    importer_chefe = ImportadorChefesExecutivos(
        NOME_CURTO, 'Presidentes', 'Presidente', XML_FILE)
    importer_chefe.importar_chefes()

    from importadores import cdep_genero
    cdep_genero.main()
    logger.info('IMPORTACAO DE DADOS DA CAMARA DOS DEPUTADOS FINALIZADA')
