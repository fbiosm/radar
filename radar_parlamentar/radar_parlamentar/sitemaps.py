from django.contrib import sitemaps
from django.urls import reverse
from django.utils import timezone

class RadarSitemaps(sitemaps.Sitemap):

    changefreq = "daily"
    lastmod = timezone.now()

    def items(self):
        return ['index', 'origem', 'ogrupo', 'premiacoes', 'radar_na_midia',
                'radar_na_academia', 'votoaberto', 'blog', 'importadores',
                'genero', 'genero_matriz', 'genero_perfil_partido',
                'genero_comparativo_partidos', 'genero_historia_legislaturas',
                'genero_treemap', 'genero_futuro', 'genero_termos_nuvem',
                'django.contrib.sitemaps.views.sitemap']

    def location(self, item):
        return reverse(item)
